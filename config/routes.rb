Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # namespace :api do
    # namespace :v1, defaults: { format: :json } do
      post '/visited_links', to: 'v1/visits#create'
      get  '/visited_domains', to: 'v1/visits#show'
    # end
  # end

end

