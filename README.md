TODO: Исправить следующие замечания проверки кода:
1. весь код в контроллере;
2. нет валидации: ни входных данных (передаваемых ссылок) ни параметров (интервалов времени);
3. прочтение и понимание функции быстрее чем комментарии к ней;
4. комментарии TODO в коде и закомментированные куски в тестах;
5. тесты используют Timecop и зависят друг от друга и порядка (кейсы на чтение от кейсов на запись, а очистка должна запускаться всегда первой);
6. отсутствуют кейсы на получение данных за разные интервалы;
7. для вставки ссылок в редис можно было бы использовать bulk операции (например, пайплайн).

# 1 this is HTTP JSON API server (test task), it can:

## 1.1 Save visited URLs
 POST request at:
```http request
/visited_links
```
with JSON:
```json
{
    "links": [
        "https://ya.ru",
        "https://ya.ru?q=123",
        "funbox.ru",
        "https://stackoverflow.com/questions/11828270/how-to-exit-the-vim-editor"
    ]
}
```
will respond JSON:
```json
{
    "status": "ok"
}
```            

## 1.2. return visited uniq domains list in requested time interval
GET request at:
```http request
/visited_domains?from=1545221231&to=1545217638
```
will respond JSON:
```json
{
    "domains": [
        "ya.ru",
        "funbox.ru",
        "stackoverflow.com"
    ],
    "status": "ok"
}
```

# 2 How to install and run (linux)

## 2.1 Install Ruby 2.7.2
 - by rbenv http://rbenv.org/
```sh
# install rbenv from https://github.com/rbenv/rbenv-installer#rbenv-installer
curl -fsSL https://github.com/rbenv/rbenv-installer/raw/master/bin/rbenv-installer | bash
curl -fsSL https://github.com/rbenv/rbenv-installer/raw/master/bin/rbenv-doctor | bash

# after install rbenv, install ruby 2.7.2
rbenv install 2.7.2
rbenv global 2.7.2
ruby -v
```
 - or RVM https://rvm.io/

## 2.2 Install and run Redis server
 - from source
```shell script
wget https://download.redis.io/releases/redis-6.0.10.tar.gz
tar xzf redis-6.0.10.tar.gz
cd redis-6.0.10
make
src/redis-server
```
 - other
```sh
https://hub.docker.com/_/redis/
```
```sh
urpmi redis
```

## 2.3 Clone Rails App, install dependencies, run tests and app
```sh
git clone https://gitlab.com/pavel-suho/fun-box-test-task.git
cd fun-box-test-task
bundle
bundle exec rspec spec/requests
rails server
``` 

## 3 Use (with Insomnia REST Client)
- Download and install insomnia from: https://dl.bintray.com/getinsomnia/Insomnia/:insomnia_7.1.1_amd64.deb 
- Run Insomnia
- Go to menu:
>           Application->
>               Preferences->
>                   Data->
>                       Import data->
>                           from file

- Point to file Insomnia_URLS.json located in app folder
- use it for make GET/POST requests