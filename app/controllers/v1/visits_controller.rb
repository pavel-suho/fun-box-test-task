module V1
  class VisitsController < ApplicationController

    # POST /visited_links
    # Post links to redis
    #
    # @param(body/json)
    #           # {
    #           #     "links": [
    #           #         "https://ya.ru",
    #           #         "https://ya.ru?q=123",
    #           #         "funbox.ru",
    #           #         "https://stackoverflow.com/questions/11828270/how-to-exit-the-vim-editor"
    #           #     ]
    #           # }
    #
    # @return(http) [JSON]
    #   - { "status":[String]("ok"|"error") }
    #
    def create
      now = Time.now
      visits_time = now.to_i
      # value must be uniq: https://redis.io/commands/zadd#elements-with-the-same-score then float
      link_uniq_prefix = now.to_f.to_s.concat(' ')

      links_params.each do |raw_link|
        # Possibly corrupted data, and no need to add a prefix.
        # TODO: consult for 'links without proto is OK?', now auto fixed
        link = auto_fix_link_protocol(raw_link)
        return render json: { status: 'error' } unless $redis.zadd(
          Rails.configuration.redis_key_name, visits_time, link_uniq_prefix + link, nx: true
        )
      end

      render json: { status: 'ok' }
    end

    # GET /visited_domains?from=1234567890&to=9874563210
    # Return a unique domains (host part of URL) list of visited links in unix-time interval.
    #   https://en.wikipedia.org/wiki/Unix_time
    #   https://unixtime-converter.com/
    #
    # @example Retrieve domains from 13-Feb-2009 23:31:30 to 29-Nov-2282 22:13:30
    #   /visited_domains?from=1234567890&to=9874563210
    #     # =>  {
    #     # =>      "domains": [
    #     # =>          "funbox.ru",
    #     # =>          "stackoverflow.com",
    #     # =>          "ya.ru"
    #     # =>      ],
    #     # =>      "status": "ok"
    #     # =>  }
    #
    # @param(url) [String] from
    #   - inclusive seconds since the Epoch
    # @param(url) [String] to
    #   - inclusive seconds since the Epoch
    #
    # @return(http) [JSON]
    #   - { "domains":[Array<String>], "status":[String]("ok"|"error") }
    #
    def show
      # TODO: consult for expected result size, possible set limit:     ,:limit => [0, 1024])
      links = $redis.zrangebyscore(Rails.configuration.redis_key_name, from_to_params[:from], from_to_params[:to])

      uniq_hosts = links.map { |link| URI.parse(link.split[1]).host }.compact.sort.uniq

      render json: { domains: uniq_hosts, status: 'ok' }
    end

    private

    # Return true if 'what' param value not starts with 'with' param value
    #
    # @param [String] what
    #   - where to lookup start substring
    # @param [String] with
    #   - what substring trying to find
    #
    # @return [Boolean]
    #   - true if 'what' param value NOT starts with 'with' param value
    #     false if 'what' param value STARTS with 'with' param value
    #
    def not_begins_with(what, with)
      what[0..with.length - 1] != with
    end

    # Add the 'http://'' prefix to the link if it doesn't exist
    #
    def auto_fix_link_protocol(link)
      not_begins_with(link, 'https://') && not_begins_with(link, 'http://') ? 'http://'.concat(link) : link
    end

    def links_params
      params.require('visit').permit!['links']
    end

    def from_to_params
      params.permit(%i[from to]).to_hash.symbolize_keys
    end
  end
end
