# frozen_string_literal: true

require 'rails_helper'
require 'timecop'


# shared_examples "coolable" do |target_name|
#   it "should make #{ target_name } cool" do
#     target.make_cool
#     target.should be_cool
#   end
# end
#
# describe User do
#   it_should_behave_like "coolable", "target user" do
#     let(:target){User.new}
#   end
# end


# RSpec.shared_examples "visits post get" do |parameter|
#   # Same behavior is triggered also with either `def something; 'some value'; end`
#   # or `define_method(:something) { 'some value' }`
#   let(:something) { parameter }
#   it "uses the given parameter" do
#     expect(something).to eq(parameter)
#   end
# end
#
# RSpec.describe SomeClass do
#   include_examples "visits post get", "parameter1"
# end

RSpec.shared_examples "visits post get" do |stage_params|
  before do
    Timecop.freeze(Time.local(1970, 1, 1, 3, 0, stage_params[:time]))
  end

  after do
    Timecop.return
  end


  it "post " + stage_params[:context] do
    post "/visited_links", params: {:visit=>{:links=>stage_params[:links]}}
    json = JSON.parse(response.body)
    expect(response).to have_http_status(200)
    expect(json['status']).to eql('ok')
  end

  it 'get ' + stage_params[:context] + ' must contain appropriate hosts' do
    get '/visited_domains?from=0&to=9999999999'
    json = JSON.parse(response.body)
    expect(response).to have_http_status(200)
    expect(json['status']).to eql('ok')
    expect(json['domains']).to match_array (stage_params[:expected_domains]).uniq
  end
end

RSpec.describe V1::VisitsController, type: :request do
  describe 'json http requests' do

    test_stages_params = [
      {
        :context => 'first post',
        :time => 1,
        :links => %w[ https://www.site1.ru/login
                      http://www.sub.site2.com/logout
                      https://site3.org
                      https://site4.shop
                      https://funbox.ru ],
        :expected_domains => %w[ www.site1.ru www.sub.site2.com site3.org site4.shop funbox.ru ]
      },
      {
        :context => 'second post',
        :time => 2,
        :links => %w[ https://deti-online.com/audioskazki/rasskazy-nosova-mp3/neznayka-na-lune/ ],
        :expected_domains => %w[ www.site1.ru www.sub.site2.com site3.org site4.shop funbox.ru deti-online.com ]
      },
      {
        :context => 'third post',
        :time => 3,
        :links => %w[ https://ya.ru
                      https://ya.ru?q=123
                      funbox.ru
                      https://stackoverflow.com/questions/11828270/how-to-exit-the-vim-editor ],
        :expected_domains => %w[ www.site1.ru www.sub.site2.com site3.org site4.shop funbox.ru deti-online.com ya.ru stackoverflow.com ]
      }
    ]

    context 'cleanup db for test key values' do
      before do
        # DB CleanUp for redis_key_name
        $redis.zremrangebyscore(Rails.configuration.redis_key_name, '0', '9999999999')
      end

      it 'get must be empty after cleanup' do
        get '/visited_domains?from=0&to=9999999999'
        json = JSON.parse(response.body)
        expect(response).to have_http_status(200)
        expect(json['status']).to eql('ok')
        expect(json['domains']).to match_array %w[]
      end
    end

    # consistently fill in the database and check
    test_stages_params.each do |stage_params|
      include_examples "visits post get", stage_params
    end

  end
end



